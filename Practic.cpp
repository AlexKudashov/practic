﻿#include <iostream>
#include <string>
using namespace std;

class Array {
private:
    int length;					// Количество элементов в массиве
    int* arr;					// Массив элементов
public:
    // Заполнение массива
    int fill_array() {
        cout << "Input array: " << endl;
        for (int i = 0; i < length; ++i) {
            string temp;
            cin >> temp;			// Ввод элементов массива
            try {					// Проверка числа
                arr[i] = stoi(temp);
            }
            catch (invalid_argument e) {	// Ошибка, если введено не число
                cout << "The array element must be a number\n";
                return 1;
            }
        }
        return 0;
    }

    // Вывод массива
    void show_array() {
        cout << " index | element\n-------------------\n";
        for (int i = 0; i < length; ++i)
            cout << "|   " << i << "\t| " << arr[i] << "\t  |\n";
    }

    // Замена половин массива
    void change() {
        int temp;	// Для временного хранения заменяемого элемента массива
        for (int i = 0; i < length / 2; i++) {
            swap(arr[i], arr[i + length / 2]);
            //temp = arr[i];
            //arr[i] = arr[i + length / 2];
            //arr[i + length / 2] = temp;
        }
    }

    // Конструктор
    Array(int length) {
        this->length = length;
        arr = new int[length];
    }

    // Деструткор
    ~Array() {
        delete[] arr;
    }
};

int main() {
    cout << "Input length array: \n";
    string n;					// Количество элементов в массиве
    cin >> n;
    try {						// Проверка числа
        int ni = stoi(n);
        if (ni < 1) {				// Проверка положительности числа
            cout << "The length of the array must be positive!\n";
            return -1;
        }
        else if (ni % 2 != 0) {		// Проверка четности числа
            cout << "The length of the array must be an even number!\n";
            return -1;
        }
        Array a(ni);
        if (a.fill_array()) return -1;	// Заполнение массива
        cout << endl << endl << "Your array: \n ";
        a.show_array();				// Вывод исходного массива
        a.change();				// Замен половин массива
        cout << endl << endl << "Result array: \n ";
        a.show_array();				// Вывод обработанного массива
    }
    catch (invalid_argument e) {		// Ошибка, если введено не число
        cout << "The length of the array must be a number\n";
        return -1;
    }
    return 0;
}
